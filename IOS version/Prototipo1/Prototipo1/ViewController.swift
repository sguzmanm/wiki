//
//  ViewController.swift
//  Prototipo1
//
//  Created by DAVID PATIÐO MONTOYA on 28/02/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//

import UIKit
import UserNotifications
import CoreBluetooth
import CoreData


class ViewController: UIViewController {
    
    var observations = [Observacion]()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge], completionHandler: {didAllow, error in })
        
        let fetchRequest : NSFetchRequest<Observacion> = Observacion.fetchRequest()
        do{
            let observaciones = try AppDelegate.context.fetch(fetchRequest)
            self.observations = observaciones
            self.tableView.reloadData()
        }
        catch {}
        
    }
    
    @IBAction func action(_ sender: Any) {
        let content = UNMutableNotificationContent()
        content.title = "Alerta enviada"
        content.subtitle = "Alerta enviada"
        content.body = "Alerta enviada"
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)

    }
    
    @IBAction func guardar(_ sender: Any){
        let lat = 4.606
        let long = -74.071
        let UV = Int.random(in: 0 ..< 10)
        let co2 = Int.random(in: 0 ..< 10)
        print(lat)
        print(long)
        print(UV)
        print(co2)
        let observacion =  Observacion(context: AppDelegate.context)
        observacion.co2 = Float(co2)
        observacion.uv = Float (UV)
        observacion.lat = Float (lat)
        observacion.long = Float (long)
        
        AppDelegate.saveContext()
        self.observations.append(observacion)
        self.tableView.reloadData()
    }
    
    
    func  createAlert (title: String, message: String){
        

    }

        
}

extension ViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return observations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = String("Lat ")+String(observations[indexPath.row].lat)
                                  + String(" Long: ")+String(observations[indexPath.row].long)
        cell.detailTextLabel?.text=String("UV: ")+String (observations[indexPath.row].uv) + String(" CO2: ")+String(observations[indexPath.row].co2)
        
        return cell
    }
    
    
}

