//
//  Data+CoreDataProperties.swift
//  Prototipo1
//
//  Created by Ana Quintero Ossa on 4/04/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//
//

import Foundation
import CoreData


extension Data {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Data> {
        return NSFetchRequest<Data>(entityName: "Data")
    }

    @NSManaged public var lat: Float
    @NSManaged public var uv: Float
    @NSManaged public var long: Float
    @NSManaged public var creation: Date?
    @NSManaged public var co2: Float

}
