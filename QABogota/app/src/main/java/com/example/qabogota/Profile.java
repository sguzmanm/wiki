package com.example.qabogota;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Output;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.UUID;

public class Profile extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    private TextView yp;
    private TextView aq;
    private TextView uv;
    private int clickcount;
    Handler bluetoothIn;
    final int handlerState = 0;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder DataStringIN = new StringBuilder();
    private ConnectedThread MyConnectionBT;
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = null;
    private double latitude;
    private double longitude;
    private File testData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        yp = findViewById(R.id.yourPoints);
        aq = findViewById(R.id.aqLevel);
        uv = findViewById(R.id.uvLevel);
        clickcount=0;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    DataStringIN.append(readMessage);

                    int endOfLineIndex = DataStringIN.indexOf("#");

                    if (endOfLineIndex > 0) {
                        String dataInPrint = DataStringIN.substring(0, endOfLineIndex);
                        String[] aux = dataInPrint.split(",");
                        aq.setText(aux[0]);
                        uv.setText(aux[1]);
                        gps();
                        double aqdoub = Double.parseDouble(aux[0]);
                        double uvdoub = Double.parseDouble(aux[1]);
                        Register(aqdoub,uvdoub,latitude,longitude);
                        DataStringIN.delete(0, DataStringIN.length());
                    }
                }
            }
        };
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            File tarjetaSD = Environment.getExternalStorageDirectory();
            Toast.makeText(getApplicationContext(),tarjetaSD.getPath(), Toast.LENGTH_LONG).show();
            testData = new File(tarjetaSD.getPath(), "pruebaDatos");
        }
        catch (Exception e)
        {

        }
    }

    private void Register(double aq, double uv, double latitude, double longitude){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "data2",null,1);
        SQLiteDatabase database = admin.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("AQ",aq);
        cv.put("UV",uv);
        cv.put("latitude",latitude);
        cv.put("longitude",longitude);
        database.insert("contamination_data2",null, cv);
        database.close();
    }


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException
    {
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Intent intent = getIntent();
        address = intent.getStringExtra(DispositivoBT.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try
        {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
        }
        try
        {
            btSocket.connect();
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {}
        }
        MyConnectionBT = new ConnectedThread(btSocket);
        MyConnectionBT.start();
    }

    private class ConnectedThread extends Thread
    {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket)
        {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run()
        {
            byte[] buffer = new byte[256];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    String readMessage = new String(buffer, 0, bytes);
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }
    }

    public void panic(View v) {
        clickcount++;
        if(clickcount==2)
        {
            //first time clicked to do this
            Toast.makeText(getApplicationContext(),"Don't worry, we have noticed it!", Toast.LENGTH_LONG).show();
            clickcount=0;
        }
    }

    public void alert(View view)
    {
        Intent warning = new Intent(this, Warning.class);
        startActivity(warning);
    }

    public void logOut(View view)
    {
        Intent logOut = new Intent(this, MainActivity.class);
        startActivity(logOut);
    }

    public void viewData(View view)
    {
        Intent viewdata = new Intent(this, SQLiteData.class);
        startActivity(viewdata);
    }

    public void scan(View view)
    {
        try{
            OutputStreamWriter arch = new OutputStreamWriter(openFileOutput("pruebaDatos", Activity.MODE_PRIVATE));
            arch.write("Yuca");
            arch.flush();
            arch.close();
        }
        catch (Exception e)
        {

        }
    }

    @SuppressLint("MissingPermission")
    public void gps() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                });
    }
}
