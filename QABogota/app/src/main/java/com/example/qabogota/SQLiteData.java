package com.example.qabogota;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class SQLiteData extends AppCompatActivity {

    private ListView listview;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);
        ArrayList<String> list = new ArrayList<String>();
        AdminSQLiteOpenHelper admin =new AdminSQLiteOpenHelper(this, "data2", null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        Cursor cursor = database.rawQuery("select * from contamination_data2",null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String name = "Air quality: "+cursor.getString(1)  + " UV radiation: " +cursor.getString(2) + " Latitude: " + cursor.getString(3) + " Longitude: " + cursor.getString(4);
                list.add(name);
                cursor.moveToNext();
            }
        }
        listview =  findViewById(R.id.lv);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);
    }
    @Override
    public void onResume(){
        super.onResume();
    }


}
